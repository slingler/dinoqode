
## Build and push images

```
docker buildx build --platform linux/arm64,linux/arm/v7,linux/arm/v6 -t slingler/dinoqode_qrplay:latest -f Dockerfile-qrplay --push .
docker buildx build --platform linux/arm64,linux/arm/v7,linux/arm/v6 -t slingler/dinoqode_web:latest -f Dockerfile-web --push .
docker buildx build --platform linux/arm64,linux/arm/v7,linux/arm/v6 -t slingler/dinoqode_api:latest -f Dockerfile-api --push .
```
